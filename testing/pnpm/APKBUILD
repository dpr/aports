# Contributor: Hygna <hygna@proton.me>
# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Hygna <hygna@proton.me>
pkgname=pnpm
pkgver=8.5.1
pkgrel=0
pkgdesc="Fast, disk space efficient package manager"
url="https://pnpm.io"
arch="noarch"
license="MIT"
depends="nodejs"
source="https://registry.npmjs.org/pnpm/-/pnpm-$pkgver.tgz"
options="!check" # not implemented
builddir="$srcdir/package"

prepare() {
	default_prepare

	# remove node-gyp
	rm -rf dist/node-gyp-bin dist/node_modules/node-gyp
	# remove windows files
	rm -rf dist/vendor/*.exe

	# remove other unnecessary files
	find . -type f \( \
		-name '.*' -o \
		-name '*.cmd' -o \
		-name '*.bat' -o \
		-name '*.map' -o \
		-name '*.md' -o \
		-name 'LICENSE*' -o \
		-name 'license' -o \
		-name 'README*' -o \
		-name 'readme.markdown' \) -delete
}

package() {
	local DESTDIR="/usr/share/node_modules/pnpm"

	mkdir -p "$pkgdir$DESTDIR"
	cp -R "$builddir"/* "$pkgdir$DESTDIR"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sf ../share/node_modules/pnpm/bin/pnpm.cjs "$pkgdir"/usr/bin/pnpm
	ln -sf ../share/node_modules/pnpm/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx
}

sha512sums="
5ba7a52fb370c346bf302202933a646f15ba7fdf5342e5f80ee2680e35a9dfd5f4f0f283904a60e1c823ddde84b606000dc7505a5fde33c35d94b255137460a4  pnpm-8.5.1.tgz
"
