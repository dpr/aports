# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.31.5
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="
	cargo
	cargo-auditable
	curl-dev
	dbus-dev
	lm-sensors-dev
	notmuch-dev
	openssl-dev>3
	pulseaudio-dev
	"
options="net"
provides="i3status-rs=$pkgver-r$pkgrel"
subpackages="$pkgname-doc"
source="
	https://github.com/greshake/i3status-rust/archive/refs/tags/v$pkgver/i3status-rust-v$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/i3status-rs/i3status-rs-$pkgver.1
	"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --all-features
}

package() {
	install -Dm755 target/release/i3status-rs -t "$pkgdir"/usr/bin/

	install -Dm644 "$srcdir"/i3status-rs-$pkgver.1 \
		"$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 files/themes/* -t "$pkgdir"/usr/share/i3status-rust/themes/
	install -Dm644 files/icons/* -t "$pkgdir"/usr/share/i3status-rust/icons/
}

sha512sums="
59fdcc514f0fcf0bbb9d5e4fd618f7af482a92448adaacfc2eb5af95d4ff7887971a421fd19a6f660e1fda9a09912251d3f999bc963167a999cf5d5d35e76f11  i3status-rust-v0.31.5.tar.gz
fe1035e9f3e6e7c492449aaf24a4af10e63763fd7ff083040464cff6e19edc8e17c5ab8e37ce6a47282c0079f0ed2470259fd1474a11353bd42a58be724f91f1  i3status-rs-0.31.5.1
"
