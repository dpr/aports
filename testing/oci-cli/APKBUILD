# Contributor: Adam Bruce <adam@adambruce.net>
# Maintainer: Adam Bruce <adam@adambruce.net>
pkgname=oci-cli
pkgver=3.26.0
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure CLI"
url="https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm"
arch="noarch"
license="UPL-1.0 OR Apache-2.0"
depends="
	python3
	py3-arrow
	py3-certifi
	py3-click
	py3-cryptography
	py3-dateutil
	py3-jmespath
	py3-oci
	py3-openssl
	py3-prompt_toolkit
	py3-six
	py3-terminaltables
	py3-tz
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/oracle/oci-cli/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # Cannot test as OCI resource identifiers are required as environment variables

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
bf456f317f3255c7ee82933efb9c827c900f827e1b71f6a5c6c91671a1e0528bebf65848be1a1e9710744973fdf2d3004bbb4ec7182b490ec6a07bc66cd57d86  oci-cli-3.26.0.tar.gz
"
