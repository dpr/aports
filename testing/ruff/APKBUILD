# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.270
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/charliermarsh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	# shadow git repo for tests
	git init

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
baf7a36d276ce034672f87c2af10a527b05fccc6ab9e0614a09e3871d72120f131092cd6f9f04957744425c1ee5152c31656a9ec635630eecff48c86c3769af2  ruff-0.0.270.tar.gz
"
