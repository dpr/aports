# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=minuet
pkgver=23.04.1
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://minuet.kde.org/"
pkgdesc="Minuet Music Education"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtquickcontrols2-dev qt5-qtsvg-dev kcoreaddons-dev ki18n-dev kcrash-dev kdoctools-dev fluidsynth-dev samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/minuet-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
4c77bb216ca9d8a9c071e5e5cf571e914dd593a8a6882b69758342a878a1dd8603be53f7cf08a61379611c7695155005dfb7063a00b9919bdd14d29fcece3c16  minuet-23.04.1.tar.xz
"
