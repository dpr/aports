# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krfb
pkgver=23.04.1
pkgrel=0
# armhf, s390x and riscv64 blocked by kwallet-dev and kxmlgui-dev
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/internet/org.kde.krfb"
pkgdesc="Desktop sharing"
license="GPL-3.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kpipewire-dev
	kwallet-dev
	kwayland-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libvncserver-dev
	pipewire-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-dev
	xcb-util-image-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/krfb-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9562cf66cea000efcd76bb276b9640d8d40bd47a485298b8f91042803eec7e2666dec0a809a6c6c917d8ad6ffac7b33561576b1af2f93c408f7ced952ac5a425  krfb-23.04.1.tar.xz
"
