# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=24.0.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
de7ee76e8ea71a6156d72b019c561e668b3ba2ee6c7d2a03f25d991477f54d68af76dd7f3c1f6641fbd387e33ee7f07f27c63ba06ebab1b5bad4b8bba98a0d14  super-ttc-iosevka-24.0.0.zip
9f477ce7ce7f498dfaa7415898c47c63fc894a9969a1918ac74bf58aa989bfcfe5e8897b510484cf3999a695f8d35bbb5fe2ce90ea7139fed1bdf271f14c5285  super-ttc-iosevka-aile-24.0.0.zip
7c05ffb489139c499bd8ba0ea852c96833a833771bcc5105c60b6be012674527f955b750169bf1a4ba6e62cdf17b1803cb714e6f5d64e32107b7601e81f97eef  super-ttc-iosevka-slab-24.0.0.zip
17177a905be410699f2ff95dd97be3e3f990f4219e1e712cc0e8b15e81c86b55f538465a19eace5ee46e2b6192c89042e8beccd7273e65dc6570efacc385363a  super-ttc-iosevka-curly-24.0.0.zip
59a2737674f63194f53eb1092e18dbecf27efb6aa4f5205e07164bea38108b36425b9d0f3c9e0445798812bdd3bb6c746e9fedc742ad9e44a413141235e454ed  super-ttc-iosevka-curly-slab-24.0.0.zip
"
