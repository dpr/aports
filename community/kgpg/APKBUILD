# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgpg
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi-contacts
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kgpg"
pkgdesc="A simple interface for GnuPG, a powerful encryption utility"
license="GPL-2.0-or-later"
makedepends="
	akonadi-contacts-dev
	extra-cmake-modules
	gpgme-dev
	karchive-dev
	kcodecs-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kgpg-import fails too often
	# kgpg-encrypt and kgpg-export are broken
	# del-key fails randomly
	# genkey fails on arm on builders
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kgpg-(genkey|import|encrypt|export|del-key)" -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
928c9cdc9c2d1b31a086f39b33f1488c0fe236d1eb9799e9240dc8f48ebe5f15da8d75b924428c64bd09cf1e18dcd3fc3f47ec4136db3818c0f56228f9ca6693  kgpg-23.04.1.tar.xz
"
