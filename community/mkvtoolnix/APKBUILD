# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=mkvtoolnix
pkgver=76.0
pkgrel=0
pkgdesc="Set of tools to create, edit and inspect Matroska files"
url="https://mkvtoolnix.download/index.html"
# riscv64 blocked by fatal error: boost/core/use_default.hpp: No such file or directory
arch="all !riscv64"
license="GPL-2.0-only"
makedepends="
	boost-dev
	cmark-dev
	docbook-xsl
	file-dev
	flac-dev
	gmp-dev
	gtest-dev
	libmatroska-dev
	libogg-dev
	libvorbis-dev
	pcre2-dev
	qt6-qtbase-dev
	qt6-qtmultimedia-dev
	qt6-qtsvg
	ruby
	ruby-rake
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-gui"
source="https://mkvtoolnix.download/sources/mkvtoolnix-$pkgver.tar.xz"

build() {
	export CFLAGS="$CFLAGS -flto=auto"
	export CXXFLAGS="$CXXFLAGS -flto=auto"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--without-gettext \
		--disable-update-check
	rake V=1 -j$JOBS
}

check() {
	rake V=1 -j$JOBS tests:unit
	rake V=1 -j$JOBS tests:run_unit
}

package() {
	rake DESTDIR="$pkgdir" install
}

gui() {
	pkgdesc="$pkgdesc (GUI)"
	depends="$pkgname qt6-qtsvg"

	amove usr/bin/mkvtoolnix-gui
	amove usr/share
}

sha512sums="
7be054bd5b4772e8744db358b5147d922ab46c723803b718d3eda4871f75ad69953a0a4b73f4678048702c7b711a6f9dc87156b5dc2e4f9dc588667eae03d185  mkvtoolnix-76.0.tar.xz
"
