# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knotifications
pkgver=5.106.0
pkgrel=0
pkgdesc="Abstraction for system notifications"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="BSD-3-Clause AND LGPL-2.0-or-later AND LGPL-2.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	libcanberra-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtspeech-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifications-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running dbus-daemon

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
37cb621aba002764c8df9095920eb01a045801fbdb4c80d2bb3f5ae45f2f70a8cdec8bffc47b9f267d2f29b3f9d3d346895ae3df49d3c84d54a41076c9fab3f5  knotifications-5.106.0.tar.xz
"
