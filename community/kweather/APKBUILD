# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kweather
pkgver=23.04.1
pkgrel=0
pkgdesc="Weather application for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/kweather"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by plasma-framework
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later AND CC-BY-4.0"
depends="
	kirigami-addons
	kirigami2
	kquickcharts
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	kquickcharts-dev
	kweathercore-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtcharts-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kweather-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bbc39084f461cef1d79427eb59cd1e8662876d53c740e7c3d0f08b97ecffec35cb96e7c3b4d044e8edc885fcc7f044b695e9b8838d19541d682699500acb32de  kweather-23.04.1.tar.xz
"
