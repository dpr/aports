# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpublictransport
pkgver=23.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/libraries/kpublictransport"
pkgdesc="Library to assist with accessing public transport timetables and other data"
license="BSD-3-Clause AND LGPL-2.0-or-later AND MIT"
depends_dev="
	ki18n-dev
	networkmanager-qt-dev
	protobuf-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpublictransport-$pkgver.tar.xz"
options="!check" # Broken for now
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1b48bfcaf5c748094dd599ca6dfa43b0672ac6234df12866d01d032d26fbd1f92b42454980c59071b01a8ff73050ef22b681878cb8682b41a0b0ef9d4a0c91c3  kpublictransport-23.04.1.tar.xz
"
