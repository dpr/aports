# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi
pkgver=23.04.1
pkgrel=0
pkgdesc="A cross-desktop storage service for PIM data and meta data providing concurrent read, write, and query access"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kaccounts-integration
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends="
	mariadb
	qt5-qtbase-mysql
	qt5-qtbase-sqlite
	"
depends_dev="
	boost-dev
	kaccounts-integration-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libaccounts-qt-dev
	libxml2-dev
	qt5-qtbase-dev
	shared-mime-info
	sqlite-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	cmd:dbus-run-session
	xvfb-run
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang"

build() {
	# make -dbg smaller
	export CFLAGS="$CFLAGS -g1"
	export CXXFLAGS="$CXXFLAGS -g1"
	# akonadi recurses while walking e-mail threads, so give it a decent stack
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=1024768" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	cd build
	# akonadixml-xmldocumenttest, mimetypecheckertest and akonadi-mysql-testenvironmenttest are broken
	# All sqlite tests are hanging
	# All mysql tests are broken "Cannot connect to non-local host <hostname>"
	# tagmodeltest fails on 32-bit architectures
	local skipped_tests="("
	local tests="
		akonadixml-xmldocument
		mimetypechecker
		akonadi-mysql-testenvironment
		.*sqlite.*
		.*mysql.*
		tagmodel
		"
	case "$CARCH" in
		s390x) tests="$tests
			AkonadiServer-itemcreatehandler
			AkonadiServer-relationhandler
			" ;;
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE dbus-run-session xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
952a9d0124abd776adcbc8c5654ce9154cde158ea64d494d2174e9cba4ed5526aa9a6c157e90089ccf8b25528448d0ef6aa35cc4dec7c8f0c9a3d75cb3083f3a  akonadi-23.04.1.tar.xz
"
