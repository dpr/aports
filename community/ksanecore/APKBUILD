# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksanecore
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="Library providing logic to interface scanners"
license="BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND (LGPL-2.1-only OR LGPL-3.0-only) AND LicenseRef-KDE-Accepted-LGPL"
depends_dev="
	ki18n-dev
	qt5-qtbase-dev
	sane-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksanecore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fc741406c6c780f13b850d7201e5b61da362edf3ec34ef05616b775efb7bb5718e5d5524aa2e5ca120057b712f0d6bbf2c07a5c8fc966dea393842d69fdce24c  ksanecore-23.04.1.tar.xz
"
