# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=py3-fakeredis
_pkgname=${pkgname#py3-}
pkgver=2.13.0
pkgrel=0
pkgdesc="Fake implementation of redis API for testing purposes"
url="https://pypi.org/project/fakeredis/"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-redis py3-sortedcontainers"
makedepends="py3-poetry-core py3-gpep517"
checkdepends="py3-hypothesis py3-pytest py3-pytest-mock py3-lupa"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # no tests in tarball

prepare() {
	default_prepare
	# Requires unpackaged 'aioredis'
	rm -f test/test_aioredis*.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
30335e456f7ac2498fc175f9fd154a766064a05e6eb996df9d533412525a3d4279c9359ac5e43a2b9ec9bb1d653639aa10d4e1765a4e4c1bc84f764fa1efdb42  py3-fakeredis-2.13.0.tar.gz
"
