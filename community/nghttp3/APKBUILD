# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=nghttp3
pkgver=0.11.0
pkgrel=0
pkgdesc="HTTP/3 library written in C"
url="https://github.com/ngtcp2/nghttp3"
arch="all"
license="MIT"
makedepends="cmake samurai"
checkdepends="cunit-dev"
subpackages="$pkgname-dev"
source="https://github.com/ngtcp2/nghttp3/releases/download/v$pkgver/nghttp3-$pkgver.tar.gz"

build() {
	local crossopts=
	if [ "$CBUILD" != "$CHOST" ]; then
		crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$crossopts .
	cmake --build build
}

check() {
	cmake --build build --target check
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# Contains just README.rst.
	rm -rf "$pkgdir"/usr/share/doc
}

sha512sums="
6f86ede4c9616a1ba7023225fc333557738c9d63dac58fe490157165b88ea6e9e17540fda7a1ba3b0b6c57366453c05d1112a14592fc1c07eabc70912906ffce  nghttp3-0.11.0.tar.gz
"
