# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=lego
pkgver=4.11.0
pkgrel=0
pkgdesc="Let's Encrypt client and ACME library written in Go"
url="https://github.com/go-acme/lego"
license="MIT"
arch="all !s390x" # tests fail due to network timeouts
options="net chmod-clean" # tests need network access: https://github.com/go-acme/lego/issues/560
depends="ca-certificates"
makedepends="go libcap-utils"
checkdepends="tzdata"
source="https://github.com/go-acme/lego/archive/v$pkgver/lego-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

case "$CARCH" in
	aarch64)
		# TestChallengeWithProxy/matching_X-Forwarded-Host_(multiple_fields)
		options="$options !check"
		;;
	ppc64le)
		# timeout on doing stuff with a server halfway across the world
		options="$options !check"
		;;
esac

build() {
	go build -v -ldflags "-X main.version=$pkgver" -o ./bin/lego ./cmd/lego
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./bin/lego "$pkgdir"/usr/bin/lego
	setcap cap_net_bind_service=+ep "$pkgdir"/usr/bin/lego
}

sha512sums="
a581cf6a120d4eb6854794defa7b4a45bee211b0ab6c280ae8db553ce5122dc170b7b56f00e232ddf7adf4cec55e3ea5a2879dd0887d01ecb77eb9401e2fad43  lego-4.11.0.tar.gz
"
