# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=clipboard
pkgver=0.7.1
pkgrel=1
pkgdesc="Cut, copy, and paste anything, anywhere, all from the terminal"
url="https://github.com/Slackadays/Clipboard"
arch="all"
license="GPL-3.0-only"
makedepends="cmake libx11-dev samurai wayland-dev wayland-protocols"
subpackages="$pkgname-doc $pkgname-x11 $pkgname-wayland"
source="$pkgname-$pkgver.tar.gz::https://github.com/Slackadays/Clipboard/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/Clipboard-$pkgver"
options="!check" # No test configuration file

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -DNDEBUG" \
	CXXFLAGS="$CXXFLAGS -DNDEBUG" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DNO_LTO=1 \
		$CMAKE_CROSSOPTS
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

x11() {
	pkgdesc="$pkgdesc (x11 clipboard integration)"
	# pretty much always installed anyway
	install_if="$pkgname=$pkgver-r$pkgrel libx11"

	amove usr/lib/libcbx11.so
}

wayland() {
	pkgdesc="$pkgdesc (wayland clipboard integration)"
	install_if="$pkgname=$pkgver-r$pkgrel wayland-libs-server"

	amove usr/lib/libcbwayland.so
}

sha512sums="
29c50c73d5b6860bc49c41fccbd04a7b8904876a2925916471039b22abb6a964d04dbbdb5fa7a97da346c85c1c47ab5b76a3f9a77ce11446c6fc7a5754c699fe  clipboard-0.7.1.tar.gz
"
