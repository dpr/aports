# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktouch
pkgver=23.04.1
pkgrel=0
# riscv64 and s390x blocked by rust
# armhf blocked by qt5-qtdeclarative
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/ktouch/"
pkgdesc="Touch Typing Tutor"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libx11-dev
	libxcb-dev
	libxkbfile-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtx11extras-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktouch-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4aa53514b467f57553da42661317a7ea8b6d81a04b3c56fdae57d612d6ccff81c23490549f2b6a2d590a13cd5429f688a962dd44e077d1f3a83cef4ba06653d  ktouch-23.04.1.tar.xz
"
