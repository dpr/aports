# Maintainer: psykose <alice@ayaya.dev>
pkgname=sequoia-sq
pkgver=0.30.0
pkgrel=3
pkgdesc="Command-line frontends for Sequoia"
url="https://gitlab.com/sequoia-pgp/sequoia-sq"
# rust-ring
arch="all !ppc64le !s390x !riscv64"
license="GPL-2.0-or-later"
makedepends="
	bzip2-dev
	cargo
	cargo-auditable
	clang-dev
	openssl-dev
	sqlite-dev
	"
checkdepends="bash"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://gitlab.com/sequoia-pgp/sequoia-sq/-/archive/v$pkgver/sequoia-sq-v$pkgver.tar.bz2"
builddir="$srcdir/sequoia-sq-v$pkgver"
options="net" # cargo deps


prepare() {
	default_prepare

	cargo update -p buffered-reader --precise 1.2.0
	cargo update -p sequoia-openpgp --precise 1.16.0
	cargo fetch --target="$CTARGET" --locked
}

build() {
	export CARGO_TARGET_DIR=target
	cargo auditable build --frozen --release --no-default-features --features crypto-openssl
}

check() {
	cargo test --frozen --no-default-features --features crypto-openssl
}

package() {
	install -Dm755 target/release/sq \
		-t "$pkgdir"/usr/bin/
	install -Dm644 target/_sq \
		"$pkgdir"/usr/share/zsh/site-functions/_sq
	install -Dm644 target/sq.bash \
		"$pkgdir"/usr/share/bash-completion/completions/sq
	install -Dm644 target/sq.fish \
		"$pkgdir"/usr/share/fish/completions/sq.fish
	install -Dm644 target/release/build/sequoia-sq-*/out/*.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
39a50e037e2be05c17cf430e59db7a3c13e34a8ea75a31a914b0301ae2669dd00dd7ef34062a34b27b24508d0ad0132b577dbe607e0beadd951595f3cce45419  sequoia-sq-v0.30.0.tar.bz2
"
