# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-pypdf
pkgver=3.9.0
pkgrel=0
pkgdesc="Pure-Python library built as a PDF toolkit"
url="https://github.com/py-pdf/pypdf"
arch="noarch"
license="BSD-3-Clause"
depends="python3"
options="!check" # issues with reading pdf files from test dirs
makedepends="py3-gpep517 py3-installer py3-flit-core"
checkdepends="py3-pillow py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/py-pdf/pypdf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/pypdf-$pkgver"

provides="py3-pypdf2=$pkgver-r$pkgrel"
replaces="py3-pypdf2"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
da9489bfd7c91c497852ce71050548d99fbca8118575c5eec85d55611fdafbe471c1cf7c4c8ef0e46165a89771e557c868f133af325d4672ba897fffffd43c02  py3-pypdf-3.9.0.tar.gz
"
