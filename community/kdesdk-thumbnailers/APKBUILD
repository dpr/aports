# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesdk-thumbnailers
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/development/"
pkgdesc="Plugins for the thumbnailing system"
license="GPL-2.0-only OR GPL-3.0-only"
makedepends="
	extra-cmake-modules
	gettext-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-thumbnailers-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cbdff7808b9f6a08404613d08f1a4267c4b3ba0deac908aba6bbc39d86f0aa4b2971217d1303918f3386f1607ddc4dc3d0a24966034d7b9d2ec58d9157fce69d  kdesdk-thumbnailers-23.04.1.tar.xz
"
