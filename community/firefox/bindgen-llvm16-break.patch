Patch-Source: https://phabricator.services.mozilla.com/D178251
https://bugzilla.mozilla.org/show_bug.cgi?id=1831242
--
diff --git a/Cargo.lock b/Cargo.lock
--- a/Cargo.lock
+++ b/Cargo.lock
@@ -423,12 +423,10 @@
 ]
 
 [[package]]
 name = "bindgen"
 version = "0.64.0"
-source = "registry+https://github.com/rust-lang/crates.io-index"
-checksum = "c4243e6031260db77ede97ad86c27e501d646a27ab57b59a574f725d98ab1fb4"
 dependencies = [
  "bitflags 1.3.2",
  "cexpr",
  "clang-sys",
  "lazy_static",
diff --git a/Cargo.toml b/Cargo.toml
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -120,11 +120,11 @@
 
 # Patch wasi 0.10 to 0.11
 wasi = { path = "build/rust/wasi" }
 
 # Patch bindgen 0.63 to 0.64
-bindgen = { path = "build/rust/bindgen" }
+bindgen_0_63 = { package = "bindgen", path = "build/rust/bindgen" }
 
 # Patch memoffset 0.6 to 0.8
 memoffset = { path = "build/rust/memoffset" }
 
 # Patch ntapi 0.3 to 0.4
@@ -142,10 +142,13 @@
 wasm-bindgen = { path = "build/rust/dummy-web/wasm-bindgen" }
 web-sys = { path = "build/rust/dummy-web/web-sys" }
 
 # Overrides to allow easier use of common internal crates.
 moz_asserts = { path = "mozglue/static/rust/moz_asserts" }
+
+# Patch bindgen to work around issues with some unsound transmutes when compiling with LLVM 16+.
+bindgen = { path = "third_party/rust/bindgen" }
 
 # Patch `rure` to disable building the cdylib and staticlib targets
 # Cargo has no way to disable building targets your dependencies provide which
 # you don't depend on, and linking the cdylib breaks during instrumentation
 # builds.
diff --git a/supply-chain/config.toml b/supply-chain/config.toml
--- a/supply-chain/config.toml
+++ b/supply-chain/config.toml
@@ -20,10 +20,14 @@
 url = "https://raw.githubusercontent.com/mozilla/supply-chain/main/audits.toml"
 
 [policy.autocfg]
 audit-as-crates-io = true
 notes = "This is the upstream code plus a few local fixes, see bug 1685697."
+
+[policy.bindgen]
+audit-as-crates-io = false
+notes = "This is a local override of the bindgen crate from crates.io, with a small local patch."
 
 [policy.chardetng]
 audit-as-crates-io = true
 notes = "This is a crate Henri wrote which is also published. We should probably update Firefox to tip and certify that."
 
diff --git a/third_party/rust/bindgen/codegen/mod.rs b/third_party/rust/bindgen/codegen/mod.rs
--- a/third_party/rust/bindgen/codegen/mod.rs
+++ b/third_party/rust/bindgen/codegen/mod.rs
@@ -139,16 +139,17 @@
 
 fn derives_of_item(
     item: &Item,
     ctx: &BindgenContext,
     packed: bool,
+    forward_decl: bool,
 ) -> DerivableTraits {
     let mut derivable_traits = DerivableTraits::empty();
 
     let all_template_params = item.all_template_params(ctx);
 
-    if item.can_derive_copy(ctx) && !item.annotations().disallow_copy() {
+    if item.can_derive_copy(ctx) && !item.annotations().disallow_copy() && !forward_decl {
         derivable_traits |= DerivableTraits::COPY;
 
         if ctx.options().rust_features().builtin_clone_impls ||
             !all_template_params.is_empty()
         {
@@ -989,11 +990,11 @@
 
                         let mut attributes =
                             vec![attributes::repr("transparent")];
                         let packed = false; // Types can't be packed in Rust.
                         let derivable_traits =
-                            derives_of_item(item, ctx, packed);
+                            derives_of_item(item, ctx, packed, false);
                         if !derivable_traits.is_empty() {
                             let derives: Vec<_> = derivable_traits.into();
                             attributes.push(attributes::derives(&derives))
                         }
 
@@ -2030,12 +2031,13 @@
                 })
             }
         }
 
         if forward_decl {
+            let prefix = ctx.trait_prefix();
             fields.push(quote! {
-                _unused: [u8; 0],
+                _unused: ::#prefix::cell::UnsafeCell<[u8; 0]>,
             });
         }
 
         let mut generic_param_names = vec![];
 
@@ -2093,11 +2095,11 @@
                     #[repr(align(#explicit))]
                 });
             }
         }
 
-        let derivable_traits = derives_of_item(item, ctx, packed);
+        let derivable_traits = derives_of_item(item, ctx, packed, self.is_forward_declaration());
         if !derivable_traits.contains(DerivableTraits::DEBUG) {
             needs_debug_impl = ctx.options().derive_debug &&
                 ctx.options().impl_debug &&
                 !ctx.no_debug_by_name(item) &&
                 !item.annotations().disallow_debug();
@@ -3125,11 +3127,11 @@
             attrs.push(attributes::must_use());
         }
 
         if !variation.is_const() {
             let packed = false; // Enums can't be packed in Rust.
-            let mut derives = derives_of_item(item, ctx, packed);
+            let mut derives = derives_of_item(item, ctx, packed, false);
             // For backwards compat, enums always derive
             // Clone/Eq/PartialEq/Hash, even if we don't generate those by
             // default.
             derives.insert(
                 DerivableTraits::CLONE |

