# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksieve
pkgver=23.04.1
pkgrel=1
pkgdesc="KDE PIM library for managing sieves"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-only"
depends_dev="
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kimap-dev
	kio-dev
	kmailtransport-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksieve-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# libksieveui-findbar-findbarbasetest, sieveeditorhelphtmlwidgettest and sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidgettest require OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(libksieveui-findbar-findbarbase|sieveeditorhelphtmlwidget|sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidget)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
18406b990c9ba2a39be799a904b54acfc6bb0ed4fd72ce99d8a63f9c42eea83971962a43d83aafe6ae89013a157bb214f2e87fb46bea065df39d7b5fc1ffff77  libksieve-23.04.1.tar.xz
"
