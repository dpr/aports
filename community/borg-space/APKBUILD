# Contributor: Andy Hawkins <andy@gently.org.uk>
# Maintainer: Andy Hawkins <andy@gently.org.uk>
pkgname=borg-space
pkgver=2.0
pkgrel=0
pkgdesc="Report and track the size of your Borg repositories"
url="https://pypi.org/project/borg-space/"
license="GPL-3.0-or-later"
# riscv64: py3-matplotlib missing
# armhf: emborg
arch="noarch !armhf !riscv64"
depends="
	python3
	py3-appdirs
	py3-arrow
	py3-docopt
	py3-inform
	py3-matplotlib
	py3-nestedtext
	py3-quantiphy
	"
makedepends="
	py3-flit-core
	py3-gpep517
	py3-installer
"
subpackages="$pkgname-pyc"
source="https://github.com/KenKundert/borg-space/archive/v$pkgver/borg-space-$pkgver.tar.gz"
options="!check" # no test suite

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/borg_space-$pkgver*-py3-none-any.whl
}

sha512sums="
c9a5e64613b4480d1d7f180c3b49a825752fae7f4b5c2b519c4058aa11f5ee044b5e4ba951e54d1e96aa54db907071e7d053610e1d0f2a3c295883274e52525d  borg-space-2.0.tar.gz
"
