# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=gn
pkgver=0_git20230518
pkgrel=0
_commit=e9e83d9095d3234adf68f3e2866f25daf766d5c7
pkgdesc="Meta-build system that generates build files for Ninja"
arch="all"
url="https://gn.googlesource.com/gn"
license="BSD-3-Clause"
depends="samurai"
makedepends="python3"
# archive needs to include .git for the build script to be able to determine the version
source="https://dev.alpinelinux.org/archive/gn/gn-$_commit.tar.xz"
builddir="$srcdir/gn"

[ "$CARCH" = "riscv64" ] && options="$options textrels"

_disturl="dev.alpinelinux.org:/archive/gn/"
snapshot() {
	clean
	deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone https://gn.googlesource.com/gn
	tar c gn | xz -T0 -9 -e -vv - > "$SRCDEST"/gn-$_commit.tar.xz
	rsync --progress -La "$SRCDEST"/gn-$_commit.tar.xz $_disturl
}

build() {
	python3 ./build/gen.py \
		--no-static-libstdc++ \
		--no-strip \
		--allow-warnings
	ninja -C out
}

check() {
	./out/gn_unittests
}

package() {
	install -Dm755 out/gn "$pkgdir"/usr/bin/gn
}

sha512sums="
d66d08d6d69f78a4509ca70ec4ca9feab43502f48b562ace1b86db507e36d0aab828ad74c755051368f0332bbe6380575a53824e9b967d35fe0a20783ff18dbc  gn-e9e83d9095d3234adf68f3e2866f25daf766d5c7.tar.xz
"
