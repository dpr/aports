# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfig
pkgver=5.106.0
pkgrel=0
pkgdesc="Configuration system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfig-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E 'kconfig(core-(kconfig|kdesktopfile)|gui-kstandardshortcutwatcher)test' -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d8f3a7fe8f265e667842b33d3a777c2aa874c9081cffbf7afd8081ec27170270e2abee702f42e61c63b7355279030266e61b4d0849d7b820746ca3f58ade33d0  kconfig-5.106.0.tar.xz
"
