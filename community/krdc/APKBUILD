# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krdc
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/krdc/"
pkgdesc="Remote Desktop Client"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="freerdp"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	knotifyconfig-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libssh-dev
	libvncserver-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c780e0d97a25c302a7a6e311a46606ae709099188c9089452f4bd2c82842cef915d0095a0e139ae5b9a7f2fc07b5ed4a7e36c0123a57ad7996d9f2e4b5417d1d  krdc-23.04.1.tar.xz
"
