# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktimer
pkgver=23.04.1
pkgrel=0
pkgdesc="A little tool to execute programs after some time"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ktimer"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktimer-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON # does not have metainfo.yaml
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9b41a21be43facaf9cf503892e4242e9df06dbdf1f394144695444595b1c953ff322827c258ae7ffb12059833e48abae79c914b31dd90a7589aa963b8bf34b98  ktimer-23.04.1.tar.xz
"
