# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=cmake
pkgver=3.26.4
pkgrel=0
pkgdesc="Cross-platform, open-source make system"
url="https://www.cmake.org/"
arch="all"
license="BSD-3-Clause"
makedepends="
	bzip2-dev
	curl-dev
	expat-dev
	libarchive-dev
	libuv-dev
	linux-headers
	ncurses-dev
	py3-sphinx
	rhash-dev
	samurai
	xz-dev
	zlib-dev
	"
checkdepends="file musl-utils"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	"
case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac
source="https://www.cmake.org/files/$_v/cmake-$pkgver.tar.gz"
options="!check"

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp.
	# Do NOT remove --no-system-jsoncpp unless you consulted
	# maintainer

	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--sphinx-man \
		--system-libs \
		--no-system-jsoncpp \
		--generator=Ninja \
		--parallel="${JOBS:-2}"
	ninja
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest
}

package() {
	DESTDIR="$pkgdir" ninja install
}

sha512sums="
fe817c8d5e247db3f0a9a58ee37c466a47220100d9e90711cd5d06c223cef87e41d1a756e75d1537e5f8cd010dcb8971cbeab4684b1ac12bcecf84bf7b720167  cmake-3.26.4.tar.gz
"
